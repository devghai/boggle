﻿namespace Boggle
{
    static class Constants
    {
        public const string CURATED_FILE_SUFFIX = "curated";
        public const int MIN_SIDE_LENGTH = 3;
        public const int MIN_WORD_LENGTH = 3;
        public const int MIN_SCORE = 1;
        public const string TWS_FILE_PATH = @"wordlists/TWL06.txt";
        public const string ZINGARELLI_FILE_PATH = @"wordlists/zingarelli2005.txt";

    }
}
