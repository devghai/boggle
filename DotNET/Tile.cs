﻿namespace Boggle
{
    public class Tile
    {
        //declaring readonly as these values will never be modified once set and compiler has ways to optimize for this.
        private readonly int _X;
        /// <summary>
        /// X co-ordinate of the tile in the board array.
        /// This is a read only property.
        /// </summary>
        public int X
        {
            get
            {
                return _X;
            }
        }

        /// <summary>
        /// Y co-ordinate of the tile in the board array.
        /// This is a read only property.
        /// </summar
        private readonly int _Y;
        public int Y
        {
            get
            {
                return _Y;
            }
        }

        /// <summary>
        /// The alphabet contained in the tile. It can be any valid letter or 'QU'.
        /// This is a read only property.
        /// </summary>
        private readonly string _Alphabet;
        public string Alphabet
        {
            get
            {
                return _Alphabet;
            }
        }

        /// <summary>
        /// Tells whether this tile has been visited or not during recursion.
        /// </summary>
        public bool IsVisited { get; set; }

        public Tile(int x, int y, string alphabet)
        {
            _X = x;
            _Y = y;
            _Alphabet = alphabet;
            IsVisited = false;
        }

        /// <summary>
        /// Copy constructor to use when using threads.
        /// </summary>
        /// <param name="t"></param>
        public Tile(Tile t)
        {
            _X = t._X;
            _Y = t._Y;
            _Alphabet = t._Alphabet;
            IsVisited = t.IsVisited;
        }
    }
}
