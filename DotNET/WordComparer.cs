﻿using System.Collections.Generic;

namespace Boggle
{
    /// <summary>
    /// Equality comparer used for HashSet. No longer used as I switched to Dictionaries.
    /// </summary>
    public class WordComparer : EqualityComparer<WordOnBoard>
    {
        public override bool Equals(WordOnBoard x, WordOnBoard y)
        {
            return string.Compare(x.Word, y.Word, true) == 0;
        }

        public override int GetHashCode(WordOnBoard obj)
        {
            int hash = 0;
            for (int i = 0; i < obj.Word.Length; i++)
            {
                hash += (int)obj.Word[i] * 10 ^ (obj.Word.Length - 1 - i);

                //cat would result in 99 * 10^2 + 97 * 10^1 + 116 * 10^0 = 10986
            }

            return hash;
        }
    }
}
