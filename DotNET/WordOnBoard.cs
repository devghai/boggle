﻿
namespace Boggle
{
    public struct WordOnBoard
    {
        public string Word;
        public int Score;

        public WordOnBoard(string word, int score)
        {
            this.Word = word;
            this.Score = score;
        }
    }
}
