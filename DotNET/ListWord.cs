﻿using System.Collections.Generic;

namespace Boggle
{
    public class ListWord
    {
        public bool IsWord;
        public Dictionary<string, ListWord> Next;

        public ListWord()
        {
            this.IsWord = false;
            this.Next = null;
        }

        public ListWord(bool isWord)
        {
            this.IsWord = isWord;
            this.Next = null;
        }

        public ListWord(bool isWord, Dictionary<string, ListWord> next)
        {
            this.IsWord = isWord;
            this.Next = next;
        }

    }
}
